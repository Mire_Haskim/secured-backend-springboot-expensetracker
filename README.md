### Description ###

Secured springboot rest application that I created to track my expenses every month.
any user with the role ROLE_ADMIN can be able to query my endpoints.


### Setup ###

you will need to implement the CommandLineRunner to prepopulate AppUser data in the database right after application startup. or put the data directly into your database.
You will also need one user with the Role "ROLE_ADMIN" to be able to access expense resources.

spring.jpa.hibernate.ddl-auto=update  => if you want spring to generate the tables for you automatically, change it to either create or create-update.


### Tools used in this web service ###

1. Spring security, JWT, and Filters for authentication and autherization.
2. Junit and Mockito for some unit testing 
3. custom/global exception handling using @ControllerAdvice
4. Maven as a build tool. I prefer Gradle for bigger projects.
5. Postman
6. Spring data jpa
7.lombok
and more....
