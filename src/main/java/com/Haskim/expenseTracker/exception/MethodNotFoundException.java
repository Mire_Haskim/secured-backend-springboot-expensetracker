package com.Haskim.expenseTracker.exception;

import org.springframework.web.HttpRequestMethodNotSupportedException;

public class MethodNotFoundException extends HttpRequestMethodNotSupportedException {

    public MethodNotFoundException(){
        super("You're using the wrong HTTP.METHOD");
    }
}
