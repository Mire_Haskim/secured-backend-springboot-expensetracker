package com.Haskim.expenseTracker.exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

@RequiredArgsConstructor
@Data
public class CustomException {

    private final String message;
    private final Throwable reason;
    private final HttpStatus status;
    private final ZonedDateTime time;



}
