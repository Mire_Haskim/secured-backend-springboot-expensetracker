package com.Haskim.expenseTracker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class RequestExceptionHandler {

    @ExceptionHandler(value = {AppUsernameExistsException.class})
    public ResponseEntity<Object> userExistsExceptionHandler(AppUsernameExistsException e){

        CustomException exception = new CustomException(e.getMessage(), e, HttpStatus.BAD_REQUEST, ZonedDateTime.now(ZoneId.of("Z")));

        return new ResponseEntity<>(exception,HttpStatus.BAD_REQUEST);
    }



    @ExceptionHandler(value = {RoleExistsException.class})
    public ResponseEntity<Object> roleExistsExceptionHandler(RoleExistsException e){

        CustomException exception = new CustomException(e.getMessage(), e, HttpStatus.BAD_REQUEST, ZonedDateTime.now(ZoneId.of("Z")));

        return new ResponseEntity<>(exception, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MethodNotFoundException.class})
    public ResponseEntity<Object> methodNotFoundExceptionHandler(MethodNotFoundException e){

        CustomException exception = new CustomException(e.getMessage(), e, HttpStatus.METHOD_NOT_ALLOWED, ZonedDateTime.now(ZoneId.of("Z")));

        ResponseEntity<Object> objectResponseEntity = new ResponseEntity<>(exception, HttpStatus.METHOD_NOT_ALLOWED);

        return objectResponseEntity;
    }


}
