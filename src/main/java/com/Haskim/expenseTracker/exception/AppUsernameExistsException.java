package com.Haskim.expenseTracker.exception;

public class AppUsernameExistsException extends RuntimeException{

    public AppUsernameExistsException(){
        super("Username exists in system already.");
    }

    public AppUsernameExistsException(String message){
        super(message);
    }

    public AppUsernameExistsException(String message, Throwable reason) {
        super(message,reason);
    }


}
