package com.Haskim.expenseTracker.exception;

public class RoleExistsException extends RuntimeException{

    public RoleExistsException(){
        super("Role exists in system already.");
    }

    public RoleExistsException(String message){
        super(message);
    }

    public RoleExistsException(String message, Throwable reason){
        super(message, reason);
    }
}
