package com.Haskim.expenseTracker.dao;

import com.Haskim.expenseTracker.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {

      // Derived query method.
      AppUser findByUsername(String username);

      @Query(value="select * from app_user u where u.id > :Id", nativeQuery=true)
      List<AppUser> findUserWithIdGreaterThan( Long Id);




}
