package com.Haskim.expenseTracker.controller;

import com.Haskim.expenseTracker.exception.AppUsernameExistsException;
import com.Haskim.expenseTracker.model.AppUser;
import com.Haskim.expenseTracker.model.Expense;
import com.Haskim.expenseTracker.service.ExpenseServiceImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/expense")
@RequiredArgsConstructor
public class ExpenseController {

    private final ExpenseServiceImplementation serviceImplementation;

    @GetMapping("/expenses")
    public ResponseEntity<List<Expense>> getExpenses(){
        return ResponseEntity.ok().body(serviceImplementation.getExpenses());

    }


    @GetMapping("/expense/{id}")
    public ResponseEntity <Optional<Expense>> getExpense(@PathVariable("id") Long id){
        return ResponseEntity.ok().body(serviceImplementation.getExpense(id));
    }

    @PostMapping("/expense/save")
    public ResponseEntity<Expense> persistUser(@RequestBody Expense expense){

        return ResponseEntity.ok().body(serviceImplementation.persistExpense(expense));
    }



}
