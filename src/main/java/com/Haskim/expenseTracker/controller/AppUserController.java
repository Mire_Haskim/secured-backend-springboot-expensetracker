package com.Haskim.expenseTracker.controller;

import com.Haskim.expenseTracker.exception.AppUsernameExistsException;
import com.Haskim.expenseTracker.exception.RoleExistsException;
import com.Haskim.expenseTracker.model.AppUser;
import com.Haskim.expenseTracker.model.Role;
import com.Haskim.expenseTracker.service.AppUserServiceImplementation;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AppUserController {

    private final AppUserServiceImplementation userService;

    @GetMapping("/users")
    public ResponseEntity<List<AppUser>> getUsers(){
        return ResponseEntity.ok().body(userService.getAppUsers());

    }

    @GetMapping("/user/{id}")
    public ResponseEntity <Optional<AppUser>> getUser(@PathVariable Long id){
        if(userService.findUserById(id) == null){
            throw new AppUsernameExistsException("user with that id exists,");
        }
        else {
            return ResponseEntity.ok().body(userService.findUserById(id));
        }
    }

    @PostMapping("/user/save")
    public ResponseEntity<AppUser> persistUser(@RequestBody AppUser user){

        if(!userService.userExistsByUsername(user.getUsername()) && user.getUsername() != null){
            return ResponseEntity.ok().body(userService.persistUser(user));
        }

        throw new AppUsernameExistsException();


    }

    @PostMapping("/role/addUser")
    public ResponseEntity<?> addRoleToAppUser(@RequestBody addRoleToUser roleToUser){
        userService.addRoleToUser(roleToUser.getUserName(), roleToUser.getRoleName());
        return ResponseEntity.ok().build();
    }

    @PostMapping("/role/save")
    public ResponseEntity<Role> persistUser(@RequestBody Role role){

        if (!userService.roleExistsByRoleName(role.getRoleName()) && role.getRoleName() != null){
            return ResponseEntity.ok().body(userService.persistRole(role));
        }

        throw new RoleExistsException();


    }


    @GetMapping("/token/refresh")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                String refresh_token = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refresh_token);
                String username = decodedJWT.getSubject();
                AppUser user = userService.getAppUser(username);
                String access_token = JWT.create()
                        .withSubject(user.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles", user.getRoles().stream().map(Role::getRoleName).collect(Collectors.toList()))
                        .sign(algorithm);
                Map<String, String> tokens = new HashMap<>();
                tokens.put("access_token", access_token);
                tokens.put("refresh_token", refresh_token);
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            }catch (Exception exception) {
                response.setHeader("error", exception.getMessage());
                response.setStatus(FORBIDDEN.value());
                //response.sendError(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("error_message", exception.getMessage());
                response.setContentType(MimeTypeUtils.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new RuntimeException("Refresh token is missing");
        }
    }


}

@Data
class addRoleToUser{

    private String userName;
    private String roleName;

    public String getUserName() {
        return userName;
    }

    public String getRoleName() {
        return roleName;
    }



}


