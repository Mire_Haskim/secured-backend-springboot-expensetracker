package com.Haskim.expenseTracker.service;

import com.Haskim.expenseTracker.model.AppUser;
import com.Haskim.expenseTracker.model.Role;

import java.util.List;

public interface AppUserService {
    AppUser persistUser(AppUser user);
    Role persistRole(Role role);
    void addRoleToUser(String userName, String roleName);
    AppUser getAppUser(String userName);
    Role getRole(String roleName);
    List<AppUser> getAppUsers();
    boolean userExistsByUsername(String username);
    boolean roleExistsByRoleName(String roleName);


}
