package com.Haskim.expenseTracker.service;

import com.Haskim.expenseTracker.model.Expense;

import java.util.List;
import java.util.Optional;

public interface ExpenseService {

    Expense persistExpense(Expense expense);
    List<Expense> getExpenses();
    Optional<Expense> getExpense(Long id);
}
