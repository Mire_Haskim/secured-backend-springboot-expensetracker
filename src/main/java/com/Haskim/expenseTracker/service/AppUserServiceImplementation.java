package com.Haskim.expenseTracker.service;

import com.Haskim.expenseTracker.dao.AppUserRepository;
import com.Haskim.expenseTracker.dao.RoleRepository;
import com.Haskim.expenseTracker.model.AppUser;
import com.Haskim.expenseTracker.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@Transactional
@Service
public class AppUserServiceImplementation implements AppUserService{

    private final AppUserRepository appUserRepository;
    private  final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AppUserServiceImplementation(AppUserRepository appUserRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder){
        this.appUserRepository = appUserRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public AppUser persistUser(AppUser user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return appUserRepository.save(user);
    }


    @Override
    public Role persistRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String userName, String roleName) {
        AppUser user = appUserRepository.findByUsername(userName);
        Role role = roleRepository.findByRoleName(roleName);

        user.getRoles().add(role);
    }

    @Override
    public AppUser getAppUser(String userName) {
        return appUserRepository.findByUsername(userName);
    }

    @Override
    public Role getRole(String roleName) {
        return roleRepository.findByRoleName(roleName);
    }

    @Override
    public List<AppUser> getAppUsers() {
        return appUserRepository.findAll();
    }

    @Override
    public boolean userExistsByUsername(String username){
        AppUser user = appUserRepository.findByUsername(username);
        if(user == null){
            return false;
        }

        return true;

    }

    @Override
    public boolean roleExistsByRoleName(String roleName){
        Role role = roleRepository.findByRoleName(roleName);
        if(role != null){
            return true;
        }

        return false;
    }

    public Optional<AppUser> findUserById(Long id){

        return appUserRepository.findById(id);
    }

    public void deleteUsers(){
        appUserRepository.deleteAll();
    }

    public void deleteRoles(){
        roleRepository.deleteAll();
    }
}
