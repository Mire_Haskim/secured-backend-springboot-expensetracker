package com.Haskim.expenseTracker.service;

import com.Haskim.expenseTracker.dao.ExpenseRepo;
import com.Haskim.expenseTracker.model.Expense;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class ExpenseServiceImplementation implements ExpenseService{

    private final ExpenseRepo expenseRepo;

    @Autowired
    public ExpenseServiceImplementation(ExpenseRepo expenseRepo) {
        this.expenseRepo = expenseRepo;
    }


    @Override
    public Expense persistExpense(Expense expense) {
        return expenseRepo.save(expense);
    }

    @Override
    public List<Expense> getExpenses() {
        return expenseRepo.findAll();
    }

    @Override
    public Optional<Expense> getExpense(Long id) {
        return expenseRepo.findById(id);
    }
}
