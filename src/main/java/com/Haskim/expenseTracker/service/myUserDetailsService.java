package com.Haskim.expenseTracker.service;

import com.Haskim.expenseTracker.dao.AppUserRepository;
import com.Haskim.expenseTracker.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class myUserDetailsService implements UserDetailsService {
    private final AppUserRepository appUserRepository;

    @Autowired
    public myUserDetailsService(AppUserRepository appUserRepository){
        this.appUserRepository = appUserRepository;
    }
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        AppUser user = appUserRepository.findByUsername(s);
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if(user != null){
            user.getRoles().forEach(role -> {
                authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
            });

        }
        return new User(user.getUsername(), user.getPassword(), authorities);
    }
}
