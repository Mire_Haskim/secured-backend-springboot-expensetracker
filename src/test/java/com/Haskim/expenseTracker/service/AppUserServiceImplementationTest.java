package com.Haskim.expenseTracker.service;

import com.Haskim.expenseTracker.dao.AppUserRepository;
import com.Haskim.expenseTracker.dao.RoleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;


@ExtendWith(MockitoExtension.class)
class AppUserServiceImplementationTest {
    @Mock
    private AppUserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private AppUserServiceImplementation userService;

    @BeforeEach
    void setUp() {
        userService = new AppUserServiceImplementation(userRepository, roleRepository, passwordEncoder);

    }

    @Test
    void testGetAppUser() {
        String username = "mire";
        userService.getAppUser(username);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        Mockito.verify(userRepository).findByUsername(argumentCaptor.capture());

        String capturedUsername = argumentCaptor.getValue();
        assert(capturedUsername).equalsIgnoreCase(username);
    }

    @Test
    void testGetRole() {
        String roleName = "ROLE_TEST";
        userService.getRole(roleName);

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        Mockito.verify(roleRepository).findByRoleName(argumentCaptor.capture());

        String capturedRoleName = argumentCaptor.getValue();
        assert(capturedRoleName).equalsIgnoreCase(roleName);


    }

    @Test
    void getAppUsers() {
        userService.getAppUsers();
        Mockito.verify(userRepository).findAll();
    }
}