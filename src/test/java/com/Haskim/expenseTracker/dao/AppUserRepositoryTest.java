package com.Haskim.expenseTracker.dao;

import com.Haskim.expenseTracker.model.AppUser;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@DataJpaTest
public class AppUserRepositoryTest {

    @Autowired
    private AppUserRepository userRepository;

    @BeforeEach
    void setUp() {
        AppUser user1 = new AppUser(1L, "mire", "123", new ArrayList<>());
        AppUser user2 = new AppUser(2L, "haskim","1234", new ArrayList<>());
        AppUser user3 = new AppUser(3L, "moe", "123", new ArrayList<>());

        List<AppUser> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        users.add(user3);

        userRepository.saveAll(users);

    }

    @AfterEach
    void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    void testFindUserWithIdGreaterThan() {
        Long id = 2L;
        List<AppUser> users =  userRepository.findUserWithIdGreaterThan(id);
        assertThat(users.size()).isEqualTo(1);

    }
}