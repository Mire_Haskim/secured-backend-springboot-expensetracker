FROM openjdk:11
LABEL maintainer ="Mire Hashim "
ARG jar = target/*.jar
COPY ${jar} expense_tracker.jar
ENTRYPOINT ["java","-jar","/expense_tracker.jar"]
